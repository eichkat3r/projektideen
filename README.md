# projektideen

ideen für coole softwareprojekte

## formular-tool

meine arbeit verlangt dass ich regelmäßig PDF formulare ausfüllen lasse um accounts zu beantragen

workflow ist folgender: 3 formulare werden von der person ausgefüllt aber immer irgendwie falsch deshalb muss ich nachhaken dann wenn ich die richtigen formulare habe schicke ich eins davon an meinen chef zur unterschrift danach schicke ich alle formulare ans sekretariat

oft sind das 3-4 formulare pro person und ich habe aktuell gefühlt wöchentlich mindestens einen solchen vorgang

und das ist super umständlich da die validierung der formulare immer durch eine natürliche person erfolgen muss

### umsetzung

* self-hosted flask server wo man formulare in ordnern hinterlegt
* eine json-datei gibt für jeden vorgang an welche daten benötigt werden und wie die daten im online-formular repräsentiert werden (checkbox, textfeld, unterschrift im canvas-element...)
* das tool stellt das formular unter einer URL bereit zb https://localhost:5000/forms/accountantrag
* optional kann festgelegt werden ob das PDF wie ein scan aussehen soll